/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio4;

import static ejercicio4.Jugador.imprimeArrayPersonas;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author ngrajales
 */
public class Ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      
        imprimeArrayPersonas(getJugadores());
    }

    private static String[] getNombreJugadores() {
        String nombreJugadores[] = new String[]{"Juan",
            "Martha", "Lucas", "Miriam", "Pedro", "Raul", "Cristina",
            "Margarita", "Daniel", "Natalia",
            "Jose", "Manuel", "Alejandra", "Mateo", "Luis"};
         Arrays.sort(nombreJugadores);
        return nombreJugadores;
    }

    private static Integer getAleatorio() {
        Random rnd = new Random();
        return rnd.nextInt(100 - 0) * 10;
    }

    private static Integer obtenerPosicionJugador() {
        Random rnd = new Random();
        return rnd.nextInt(15 - 0);
    }

    private static Jugador[] getJugadores() {
        Jugador jugadors[] = null;
        jugadors = new Jugador[20];
        for (int i = 0; i < 20; i++) {
            String[] nombreJugadores = getNombreJugadores();
            jugadors[i] = new Jugador(nombreJugadores[obtenerPosicionJugador()], getAleatorio());
        }

        return jugadors;
    }
}
