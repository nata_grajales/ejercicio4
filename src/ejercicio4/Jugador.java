/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio4;

import java.util.Arrays;

/**
 *
 * @author ngrajales
 */
public class Jugador implements Comparable<Jugador> {

    String nombre;
    int puntuacion;

    public Jugador(String nombre, int puntuacion) {
        this.nombre = nombre;
        this.puntuacion = puntuacion;
    }

    @Override
    public int compareTo(Jugador o) {
        if (puntuacion < o.puntuacion) {
            return -1;
        }
        if (puntuacion > o.puntuacion) {
            return 1;
        }
        return 0;
    }

    static void imprimeArrayPersonas(Jugador[] array) {
        //Orderna el Listado
        Arrays.sort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i].nombre + "," + array[i].puntuacion);
        }
    }
}
